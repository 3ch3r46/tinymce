3ch3r46/tinymce
===============
The TinyMCE Extension for yii framework

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist 3ch3r46/tinymce "*"
```

or add

```
"3ch3r46/tinymce": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \a3ch3r46\tinymce\TinyMCE::widget($config); ?>```

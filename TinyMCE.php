<?php
namespace a3ch3r46\tinymce;

use yii\base\View;

use yii\web\JsExpression;

use yii\helpers\Json;

use yii\web\JsonParser;

use yii\helpers\ArrayHelper;

use yii\base\Widget;

class TinyMCE extends Widget
{
	public $config = [];
	
	public $showAdvancedImageTab = true;
	
	public $theme = 'modern';
		
	public $functionName = null;
	
	public $plugins = [
		'advlist autolink lists link image charmap print preview hr anchor pagebreak',
		'searchreplace wordcount visualblocks visualchars code fullscreen',
		'insertdatetime media nonbreaking save table contextmenu directionality',
		'emoticons template paste textcolor',
	];
	
	public $toolbar = [
		'insertfile undo redo',
		'styleselect',
		'bold italic',
		'alignleft aligncenter alignright alignjustify',
		'bullist numlist outdent indent',
		'link image',
		'forecolor backcolor',
		'print preview media',
	];
	
	public $removeToolbar = [];

	public $selector = 'textarea';
	
	public $templates = [];
	
	public $height = 300;
	
	public $others = [];
	
	public function init()
	{
		$this->config = ArrayHelper::merge([
			'selector' => $this->selector,
			'theme' => $this->theme,
			'plugins' => $this->plugins,
			'templates' => $this->templates,
			'height' => $this->height,
		], $this->config);
				
		if (!empty($this->toolbar))
			$this->config['toolbar'] = implode(' | ', $this->toolbar);
		
		if (!empty($this->removeToolbar)) {
			foreach ($this->removeToolbar as $toolbar) {
				$this->config['toolbar'] = str_replace($toolbar, '', $this->config['toolbar']);
			}
		}
		
		if ($this->showAdvancedImageTab)
			$this->config['image_advtab'] = 'true';
		
		$this->config = ArrayHelper::merge($this->config, $this->others);
		
	}
	
	public function run()
	{
		$options = Json::encode($this->config);
		\a3ch3r46\tinymce\TinyMCEAsset::register(\Yii::$app->view);
		
		if (!empty($this->functionName))
			\Yii::$app->view->registerJs("function {$this->functionName}(){ tinymce.init({$options}) }");
		else
			\Yii::$app->view->registerJs("tinymce.init({$options})");
	}
}